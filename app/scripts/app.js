angular.module('angularjsZen', ['ngRoute'])
    .config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'views/participants.html',
                controller: 'ParticipantsCtrl'
            }).
            when('/participant/:participantId',{
                templateUrl: 'views/participantDetail.html',
                controller: 'ParticipantDetailCtrl'
            }).
            when('/directives', {
                templateUrl: 'views/directives.html'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);