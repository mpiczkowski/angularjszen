angular.module('angularjsZen')
    .controller('ParticipantsCtrl', function ($scope, $location, ParticipantsRepository) {
        $scope.participants = ParticipantsRepository.getAll();

        $scope.showDetails = function(participantId) {
            $location.path('/participant/' + participantId);
        };
    })
    .controller('ParticipantDetailCtrl', function($scope, $routeParams, ParticipantsRepository) {
        $scope.participant = ParticipantsRepository.get($routeParams.participantId);
    });