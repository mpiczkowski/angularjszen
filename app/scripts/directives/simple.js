angular.module('angularjsZen')
    .directive('simpleDirective', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                url: '=',
                text: '@title'
            },
            template: '<div><div><label>My url</label><input type="text" ng-model="url"></div><div><a href="{{ url }}">{{ text }}</a></div></div>'
        }
    });
