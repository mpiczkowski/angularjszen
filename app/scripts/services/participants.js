angular.module('angularjsZen')
    .factory('ParticipantsRepository', function () {
        var participants = [
            {
                id: 1,
                name: 'Adam',
                firstName: 'Adam',
                lastName: 'Testowy',
                age: 28,
                gender: 'male',
                income: 2800.4517,
                company: 'TomTom',
                email: 'adam@tomtom.com'
            },
            {
                id: 2,
                name: 'Marek',
                firstName: 'Marek',
                lastName: 'Zly',
                age: 32,
                gender: 'male',
                income: 2844.9281,
                company: 'TomTom',
                email: 'marek@tomtom.com'
            },
            {
                id: 3,
                name: 'Jarek',
                firstName: 'Jarek',
                lastName: 'Dobry',
                age: 36,
                gender: 'male',
                income: 5266.9918,
                company: 'Other',
                email: 'jarek@other.com'
            },
            {
                id: 4,
                name: 'Hania',
                firstName: 'Hania',
                lastName: 'Zadowolona',
                age: 22,
                gender: 'female',
                income:4672.8817,
                company: 'Example',
                email: 'hania@example.com'
            }
        ];

        return {
            getAll: function() {
                return participants;
            },
            get: function(id) {
                return _.find(participants, function(p) {return p.id == id});
            }
        }
    });