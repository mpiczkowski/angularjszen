'use strict';

describe('Controller: ParticipantsCtrl', function () {

    beforeEach(module('angularjsZen'));

    var ParticipantsCtrl;
    var $scope;
    var location;

    beforeEach(inject(function ($controller, $rootScope, $location) {
        $scope = $rootScope.$new();
        ParticipantsCtrl = $controller('ParticipantsCtrl', {
            $scope: $scope
        });
        location = $location;
    }));

    it('should redirect to participants detail page', function () {
        var participantId = 1;
        var participantUrl = "/participant/" + participantId;

        $scope.showDetails(participantId);

        expect(location.path()).toBe(participantUrl);
    });

    it('should get all users', function () {
        expect($scope.participants.length).toBe(4);
    });
});
