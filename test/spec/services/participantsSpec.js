'use strict';

describe('Factory: ParticipantsRepository', function () {

    beforeEach(module('angularjsZen'));

    var ParticipantsRepository;

    beforeEach(inject(function ($injector) {
        ParticipantsRepository = $injector.get('ParticipantsRepository');
    }));

    it('should get all participants', function () {
        expect(ParticipantsRepository.getAll().length).toBe(4);
    });

    it('should get detail user', function () {
        var definedUserId = 1;

        expect(ParticipantsRepository.get(definedUserId)).toBeDefined();
    });

    it('should return undefined if there is not user', function () {
        expect(ParticipantsRepository.get(-1)).toBeUndefined();
    });
});
